title: Commands
---

## `task-master`

``` bash
$ ./task-master "console command 1" "console command etc"
```

Or if using as a Composer dependency:

```bash
php vendor/bin/task-master "console command 1" "console command etc"
```

Process several console tasks in a sequential fashion and then receive results of all tasks in a single place.

![](/task-master/images/task-sequence-command.png)
