title: Documentation
---
Welcome to the Task Master documentation.

## What is `task-master`?

`task-master` has been design to execute several console tasks in a sequential fashion and then display the results of all tasks in a one single place.

![](/task-master/images/task-sequence-command.png)

## Installation

It only takes a few minutes to set up Task Master. 

### Requirements

Installing Task Master is quite easy. However, you do need to have a couple of other things installed first:

- [Git](http://git-scm.com/)
- [PHP `7.1.3 and above`](http://php.net)
- [Composer](https://getcomposer.org)
    - [Linux / Unix / OSX installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
    - [Windows installation](https://getcomposer.org/doc/00-intro.md#installation-windows)

If your computer already has these, congratulations! Just install `task-master` with composer:

``` bash
$ composer require theshopworks/task-master
```

If not, please follow the following instructions to install all the requirements.

### Install Git

- Windows: Download & install [git](https://git-scm.com/download/win).
- Mac: Install it with [Homebrew](http://mxcl.github.com/homebrew/), [MacPorts](http://www.macports.org/) or [installer](http://sourceforge.net/projects/git-osx-installer/).
- Linux (Ubuntu, Debian): `sudo apt-get install git-core`
- Linux (Fedora, Red Hat, CentOS): `sudo yum install git-core`

### Install `task-master`

Once all the requirements are installed, you can install Hexo with composer:

``` bash
$ composer require theshopworks/task-master
```
