title: Task Master 0.1 Released
---
The initial version of this application has been released tagged version [v0.1](https://packagist.org/packages/theshopworks/task-master#v0.1). You can download this via Composer.

