<?php

namespace Shopworks\TaskMaster\Tests\Unit\Process;

use Shopworks\TaskMaster\Process\Process;
use Shopworks\TaskMaster\Tests\UnitTestCase;

class ProcessTest extends UnitTestCase
{
    /**
     * @test
     */
    public function it_can_create_process_through_simple_method(): void
    {
        $this->assertInstanceOf(Process::class, (Process::simple('simple command')));
    }
}
