<?php

namespace Shopworks\TaskMaster\Tests\Unit\Process;

use Shopworks\TaskMaster\Process\Process;
use Shopworks\TaskMaster\Process\ProcessResult;
use Shopworks\TaskMaster\Tests\UnitTestCase;

class ProcessResultTest extends UnitTestCase
{
    /** @test */
    public function it_can_return_the_result_of_a_command(): void
    {
        $this->assertEquals(
            "command 1: <info>✔</info>",
            (new ProcessResult('command 1', Process::EXIT_CODE_SUCCESS, true))->getResult()
        );

        $this->assertEquals(
            "command 2: <error>✘</error>",
            (new ProcessResult('command 2', Process::EXIT_CODE_FAILED, true))->getResult()
        );
    }
}
