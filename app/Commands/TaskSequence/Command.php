<?php

namespace Shopworks\TaskMaster\Commands\TaskSequence;

use Illuminate\Support\Collection;
use Shopworks\TaskMaster\Process\Process;
use Shopworks\TaskMaster\Process\Processor;
use Shopworks\TaskMaster\Process\ProcessResult;
use Shopworks\TaskMaster\Process\ProcessResultCollection;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Command extends BaseCommand
{
    protected static $defaultName = 'task-sequence';
    protected $description = 'Process several tasks in a sequential fashion and then receive result of tasks '
        . 'in a single place.';
    /**
     * @var SymfonyStyle
     */
    protected $io;

    private $processor;
    private $process;

    public function __construct(Process $process, Processor $processor)
    {
        parent::__construct();

        $this->process = $process;
        $this->processor = $processor;
    }

    protected function configure(): void
    {
        $this
            ->setDescription($this->description)
            ->setHelp('Provide a list of tasks to execute. Separate tasks by quoting them and using spaces.')
            ->addArgument(
                'tasks',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'Please provide some tasks to execute'
            )
            ->addOption('noResults');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $tasks = Collection::make($input->getArgument('tasks'));
        $results = $this->runCommands($tasks);

        if (!$input->getOption(('noResults'))) {
            $this->compileResults($results);
        }

        if ($results->hasErrors()) {
            $this->io->error(
                "{$results->failCount()} out of {$results->count()} tasks have failed."
            );

            return Process::EXIT_CODE_FAILED;
        }

        $this->io->success("{$results->count()} out of {$results->count()} tasks have passed. Good job!");

        return Process::EXIT_CODE_SUCCESS;
    }

    private function runCommands(Collection $commands): ProcessResultCollection
    {
        return $commands->reduce(function (ProcessResultCollection $results, string $command) {
            $this->io->title("Running command: \"{$command}\"");
            $process = $this->runCommand($command);
            $this->io->newLine();

            $results->push(new ProcessResult($command, $process->getExitCode(), $process->isSuccessful()));

            return $results;
        }, ProcessResultCollection::make());
    }

    private function runCommand(string $command): Process
    {
        $process = $this->process->simple($command);

        return $this->processor->process($process, true);
    }

    private function compileResults(ProcessResultCollection $results): void
    {
        $this->io->title("Results:");
        $this->io->listing($results->map(function (ProcessResult $result) {
            return $result->getResult();
        })->toArray());
    }
}
